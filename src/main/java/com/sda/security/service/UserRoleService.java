package com.sda.security.service;

import com.sda.security.model.UserRole;
import com.sda.security.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Value("${user.default.roles}")
    private String[] defaultRoles;

    public Set<UserRole> getDefaultUserRoles() {
        Set<UserRole> roles = new HashSet<>();
        for (String role : defaultRoles) {
            roles.add(userRoleRepository.findByName(role));
        }
        return roles;
    }
}
