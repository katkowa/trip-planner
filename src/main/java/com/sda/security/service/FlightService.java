package com.sda.security.service;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOffer;
import com.sda.security.model.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class FlightService {
    Amadeus amadeus = AmadeusApi.getInstance();

    private Map<String, List<String>> getFlights(boolean toDestination) {
        Map<String, List<String>> flightsMap = new HashMap<>();
        try {
            Scanner scanner = new Scanner(new File(getClass().getClassLoader().getResource("flights.txt").getFile()));
            while (scanner.hasNext()) {
                String[] line = scanner.nextLine().split(" ");
                String origin = line[0];
                String destination = line[1];
                if (toDestination) {
                    origin = line[1];
                    destination = line[0];
                }
                if (!flightsMap.containsKey(destination)) {
                    flightsMap.put(destination, new ArrayList<>());
                }
                List<String> list = flightsMap.get(destination);
                list.add(origin);
                flightsMap.put(destination, list);
            }
        } catch (FileNotFoundException e) {
            // do nothing
        }
        return flightsMap;
    }

    public List<FlightDto> getFlights(final Trip trip, boolean isBack) {
        ArrayList<FlightDto> flightList = new ArrayList<>();
        try {
            if (getFlights(isBack).get(trip.getPlace().toString()) == null) {
                return flightList;
            }
            String origin = getFlights(isBack).get(trip.getPlace().toString()).get(0);
            FlightOffer[] flights;
            if (isBack) {
                flights = amadeus.shopping.flightOffers.get(Params
                        .with("origin", trip.getPlace().toString())
                        .and("destination", origin)
                        .and("departureDate", trip.getEndDate()));
            } else {
                flights = amadeus.shopping.flightOffers.get(Params
                        .with("destination", trip.getPlace().toString())
                        .and("origin", origin)
                        .and("departureDate", trip.getStartDate()));
            }
            for (FlightOffer offer : flights) {

                double price = offer.getOfferItems()[0].getPrice().getTotal();
                FlightOffer.Segment[] segments = offer.getOfferItems()[0].getServices()[0].getSegments();
                FlightDto flight = new FlightDto();
                flight.setPrice(price);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssXXX");
                flight.setDeparturePlace(segments[0].getFlightSegment().getDeparture().getIataCode());
                flight.setDepartureTime(segments[0].getFlightSegment().getDeparture().getAt().replace("T", " "));
                int numberOfChanges = segments.length > 1 ? segments.length - 1 : 0;
                flight.setArrivalPlace((segments[numberOfChanges].getFlightSegment().getArrival().getIataCode()));
                flight.setArrivalTime(segments[numberOfChanges].getFlightSegment().getArrival().getAt().replace("T", " "));
                flight.setNumberOfChanges(numberOfChanges);
                flightList.add(flight);
            }

        } catch (ResponseException e) {
            e.printStackTrace();
        }
        return flightList;
    }
}
