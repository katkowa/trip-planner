package com.sda.security.service;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.HotelOffer;
import com.sda.security.model.Hotel;
import com.sda.security.model.Trip;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HotelService {
    Amadeus amadeus = AmadeusApi.getInstance();

    public List<Hotel> getHotels(Trip trip) {
        List<Hotel> hotelList = new ArrayList<>();
        try {
            HotelOffer[] hotelOffers = amadeus.shopping.hotelOffers.get(Params.with("cityCode", trip.getPlace())
                    .and("checkInDate", trip.getStartDate()).and("checkOutDate", trip.getEndDate()));
            for (HotelOffer offer : hotelOffers) {
                String[] addressLines = offer.getHotel().getAddress().getLines();
                String address = "";
                for (String line : addressLines) {
                    address += line + " ";
                }

                Hotel hotel = Hotel.builder()
                        .place(trip.getPlace())
                        .name(offer.getHotel().getName())
                        .address(address)
                        .description(offer.getHotel().getDescription() != null ? offer.getHotel().getDescription().getText() : "")
                        .latitude(offer.getHotel().getLatitude())
                        .longitude(offer.getHotel().getLongitude())
                        .price(Double.parseDouble(offer.getOffers()[0].getPrice().getTotal()))
                        .build();
                hotelList.add(hotel);
            }
        } catch (ResponseException e) {
            // DO NOTHING
        }
        return hotelList;
    }



}
