package com.sda.security.service;

import com.sda.security.exception.PasswordDoNotMatchException;
import com.sda.security.model.AppUser;
import com.sda.security.model.Trip;
import com.sda.security.model.UserRole;
import com.sda.security.repository.AppUserRepository;
import com.sda.security.repository.TripRepository;
import com.sda.security.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    private final AppUserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRoleService userRoleService;
    private final TripRepository tripRepository;
    private UserRoleRepository userRoleRepository;

    @Autowired
    public UserService(final AppUserRepository userRepository, final BCryptPasswordEncoder passwordEncoder, final UserRoleService userRoleService, final TripRepository tripRepository, final UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleService = userRoleService;
        this.tripRepository = tripRepository;
        this.userRoleRepository = userRoleRepository;
    }

    public void registerUser(final String username, final String password, final String passwordConfirm) {
        if (!password.equals(passwordConfirm)) {
            throw new PasswordDoNotMatchException("Password and password confirm do not match.");
        }
        if (password.length() <= 3) {
            throw new PasswordDoNotMatchException("Password must be at least 4 characters.");
        }
        AppUser appUser = new AppUser();
        appUser.setEmail(username);
        appUser.setPassword(passwordEncoder.encode(password));
        appUser.setRoles(userRoleService.getDefaultUserRoles());

        userRepository.save(appUser);
    }

    @Transactional
    public List<AppUser> getAllUsers() {
        return userRepository.findAll();
    }

    @Transactional
    public void deleteUserById(final Long userId) {
        List<Trip> tasks = tripRepository.findByUserId(userId);
        tripRepository.deleteAll(tasks);
        userRepository.deleteById(userId);
    }

    @Transactional
    public void makeAdmin(final Long userId) {
        Optional<AppUser> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            AppUser user = userOptional.get();
            Set<UserRole> roles = user.getRoles();
            roles.add(userRoleRepository.findByName("ADMIN"));
            user.setRoles(roles);
            userRepository.save(user);
        }
    }
}
