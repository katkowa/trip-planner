package com.sda.security.service;

import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.PointOfInterest;
import com.sda.security.model.*;
import com.sda.security.repository.AppUserRepository;
import com.sda.security.repository.FlightRepository;
import com.sda.security.repository.HotelRepository;
import com.sda.security.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TripService {
    private final TripRepository tripRepository;
    private final AppUserRepository userRepository;
    private final FlightRepository flightRepository;
    private final HotelRepository hotelRepository;

    @Autowired
    public TripService(final TripRepository tripRepository, final AppUserRepository userRepository, final FlightRepository flightRepository, final HotelRepository hotelRepository) {
        this.tripRepository = tripRepository;
        this.userRepository = userRepository;
        this.flightRepository = flightRepository;
        this.hotelRepository = hotelRepository;
    }

    @Transactional
    public List<Trip> getAll() {
        return tripRepository.findAll();
    }

    @Transactional
    List<Trip> getByUserEmailAndStatus(String email, Status status) {
        return tripRepository.findByUserEmailAndStatus(email, status);
    }

    public void setStatus(final Long tripId, final Status status) {
        Optional<Trip> optionalTrip = tripRepository.findById(tripId);
        if (optionalTrip.isPresent()) {
            optionalTrip.get().setStatus(status);
            tripRepository.save(optionalTrip.get());
        }
    }

    public void save(final Trip trip, final String username) {
        Optional<AppUser> userOptional = userRepository.findByEmail(username);
        if (userOptional.isPresent()) {
            AppUser user = userOptional.get();
            user.getTrips().add(trip);
            trip.setUser(user);
            userRepository.save(user);
            tripRepository.save(trip);
        }
    }

    public List<String> getPointsOfInterestForTrip(final Place place) {
        List<String> pointsList = new ArrayList<>();
        try {
            PointOfInterest[] pointOfInterests = AmadeusApi.getInstance().referenceData.locations.pointsOfInterest.bySquare.get(
                    Params.with("north", place.getNorth())
                            .and("west", place.getWest())
                            .and("south", place.getSouth())
                            .and("east", place.getEast()));
            for (PointOfInterest point : pointOfInterests) {
                pointsList.add(point.getName());
            }
        } catch (ResponseException e) {
            // do Nothing
        }
        return pointsList;
    }

    public Optional<Trip> getById(final Long tripId) {
        return tripRepository.findById(tripId);
    }

    @Transactional
    public void addFlight(final FlightDto flightDto, final boolean isBack) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssXXX");
        Flight flight = Flight.builder()
                .arrivalPlace(flightDto.getArrivalPlace())
                .arrivalTime(LocalDateTime.parse(flightDto.getArrivalTime().replace("T", " "), formatter))
                .departurePlace(flightDto.getDeparturePlace())
                .departureTime(LocalDateTime.parse(flightDto.getDepartureTime().replace("T", " "), formatter))
                .price(flightDto.getPrice())
                .numberOfChanges(flightDto.getNumberOfChanges())
                .build();
        addFlight(flightDto.getTripId(), flight, isBack);

    }

    private void addFlight(Long tripId, Flight flight, boolean isBack) {
        Optional<Trip> tripOptional = tripRepository.findById(tripId);
        if (tripOptional.isPresent()) {
            final Flight savedFlight = flightRepository.save(flight);
            Trip trip = tripOptional.get();

            if (isBack) {
                trip.setFlightBack(flight);
            } else {
                trip.setFlightTo(savedFlight);
            }
            double totalPrice = 0;
            totalPrice += trip.getFlightBack() != null ? trip.getFlightBack().getPrice() : 0;
            totalPrice += trip.getFlightTo() != null ? trip.getFlightTo().getPrice() : 0;
            totalPrice += trip.getHotel() != null ? trip.getHotel().getPrice() : 0;
            trip.setTotalPrice();
            tripRepository.save(trip);
        }
    }

    public void addHotel(HotelDto hotelDto) {
        Optional<Trip> tripOptional = tripRepository.findById(hotelDto.getTripId());
        Hotel hotel = Hotel.builder()
                .name(hotelDto.getName())
                .address(hotelDto.getAddress())
                .description(hotelDto.getDescription())
                .place(hotelDto.getPlace())
                .price(hotelDto.getPrice())
                .latitude(hotelDto.getLatitude())
                .longitude(hotelDto.getLongitude())
                .build();
        if (tripOptional.isPresent()) {
            hotelRepository.save(hotel);
            Trip trip = tripOptional.get();
            trip.setHotel(hotel);
            trip.setTotalPrice();
            tripRepository.save(trip);
        }
    }

    public List<Trip> getToDoTrips(final String name) {
        return getByUserEmailAndStatus(name, Status.TO_DO);
    }

    public List<Trip> getInProgressTrips(final String name) {
        return getByUserEmailAndStatus(name, Status.IN_PROGRESS);
    }

    public List<Trip> getDoneTrips(final String name) {
        return getByUserEmailAndStatus(name, Status.IN_PROGRESS);
    }
}
