package com.sda.security.service;

import com.sda.security.model.AppUser;
import com.sda.security.model.UserRole;
import com.sda.security.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LoginService implements UserDetailsService {
    @Autowired
    private AppUserRepository appUserRepository;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        Optional<AppUser> optionalAppUser = appUserRepository.findByEmail(email);
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();
            String[] roles = appUser.getRoles()
                    .stream()
                    .map(UserRole::getName)
                    .toArray(String[]::new);

            return User.builder()
                    .username(appUser.getEmail())
                    .password(appUser.getPassword())
                    .roles(roles)
//                    .authorities(new ArrayList<>(getAuthorities(appUser.getRoles())))
                    .build();
        }
        throw new UsernameNotFoundException("Unable to find user with that email");
    }

    private List<GrantedAuthority> getAuthorities(final Set<UserRole> roles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (UserRole role : roles) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return grantedAuthorities;
    }


}
