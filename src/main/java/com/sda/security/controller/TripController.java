package com.sda.security.controller;

import com.sda.security.model.*;
import com.sda.security.service.FlightService;
import com.sda.security.service.HotelService;
import com.sda.security.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/trip")
public class TripController {
    private final TripService tripService;
    private final FlightService flightService;
    private final HotelService hotelService;

    @Autowired
    public TripController(final TripService tripService, final FlightService flightService, final HotelService hotelService) {
        this.tripService = tripService;
        this.flightService = flightService;
        this.hotelService = hotelService;
    }

    @GetMapping("/backlog")
    public String getBacklog(Principal principal, Model model) {
        if (principal != null) {
            List<Trip> toDoList = tripService.getToDoTrips(principal.getName());
            model.addAttribute("toDoList", toDoList);
            List<Trip> inProgressList = tripService.getInProgressTrips(principal.getName());
            model.addAttribute("inProgressList", inProgressList);
            List<Trip> doneList = tripService.getDoneTrips(principal.getName());
            model.addAttribute("doneList", doneList);
            return "backlog";
        }
        return "redirect:/login";
    }

    @PostMapping("/add")
    public String displayTripForm(Model model, Place place) {
        Trip trip = new Trip();
        if (place != null) {
            trip.setPlace(place);
        }
        model.addAttribute("newTrip", trip);

        return "tripAdd";
    }

    @PostMapping("/save")
    public String addTrip(Trip newTrip, Principal principal) {
        tripService.save(newTrip, principal.getName());
        return "redirect:/trip/backlog";
    }

    @PostMapping("/make-in-progress")
    public String makeInProgress(@RequestParam Long tripId) {
        tripService.setStatus(tripId, Status.IN_PROGRESS);
        return "redirect:/trip/backlog";
    }

    @PostMapping("/make-done")
    public String makeDone(@RequestParam Long tripId) {
        tripService.setStatus(tripId, Status.DONE);
        return "redirect:/trip/backlog";
    }

    @PostMapping("/make-to-do")
    public String makeToDo(@RequestParam Long tripId) {
        tripService.setStatus(tripId, Status.TO_DO);
        return "redirect:/trip/backlog";
    }

    @GetMapping("/dreams")
    public String getDreamsView(Principal principal, Model model) {
        if (principal == null) {
            return "redirect:/login";
        }
        model.addAttribute("toDoList", tripService.getToDoTrips(principal.getName()));
        return "dreams";
    }

    @PostMapping("/dreams")
    public String getDreamsViewWithPointsOfInterest(Principal principal, Place place, Model model) {
        if (principal == null) {
            return "redirect:/login";
        }
        model.addAttribute("toDoList", tripService.getToDoTrips(principal.getName()));
        model.addAttribute("pointsList", tripService.getPointsOfInterestForTrip(place));
        model.addAttribute("place", place);
        return "dreams";
    }

    @GetMapping("/planner")
    public String getPlanner(Principal principal, Model model) {
        if (principal == null) {
            return "redirect:/login";
        }
        model.addAttribute("inProgressList", tripService.getInProgressTrips(principal.getName()));
        return "planner";
    }

    @PostMapping("/planner")
    public String getPlannerWithAction(HttpServletRequest request, Principal principal, Model model, Long tripId) {
        if (principal == null) {
            return "redirect:/login";
        }
        if (request.getParameter("flightTo") != null) {
            model.addAttribute("pressedButton", "flightTo");
            model.addAttribute("flightList", flightService.getFlights(tripService.getById(tripId).get(), false));
        } else if (request.getParameter("flightBack") != null) {
            model.addAttribute("pressedButton", "flightBack");
            model.addAttribute("flightList", flightService.getFlights(tripService.getById(tripId).get(), true));
        } else if (request.getParameter("hotel") != null) {
            model.addAttribute("pressedButton", "hotel");
            List<Hotel> hotels = hotelService.getHotels(tripService.getById(tripId).get());
            model.addAttribute("hotelList", hotelService.getHotels(tripService.getById(tripId).get()));
        } else if (request.getParameter("info") != null) {
            model.addAttribute("pressedButton", "info");
        }
        model.addAttribute("inProgressList", tripService.getInProgressTrips(principal.getName()));
        if (tripId != null) {
            model.addAttribute("trip", tripService.getById(tripId).get());
        }
        return "planner";
    }

    @PostMapping("/flightTo")
    public RedirectView addFlightTo(Model model, FlightDto flightDto, HttpServletRequest request,
                              RedirectAttributes redirectAttr) {

        tripService.addFlight(flightDto, false);
        redirectAttr.addAttribute("tripId", flightDto.getTripId());

        redirectAttr.addAttribute("info", "info");
        request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.TEMPORARY_REDIRECT);
        return new RedirectView("/trip/planner");
    }

    @PostMapping("/flightBack")
    public RedirectView addFlightBack(Model model, String departurePlace, String arrivalPlace,
                                String departureTime, String arrivalTime,
                                double price, int numberOfChanges, HttpServletRequest request, RedirectAttributes redirectAttr) {

//        tripService.addFlight(tripId, departurePlace, departureTime, arrivalPlace, arrivalTime, price, numberOfChanges, true);
        redirectAttr.addAttribute("info", "info");
        request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.TEMPORARY_REDIRECT);
        return new RedirectView("/trip/planner");
    }

    @PostMapping("/hotel")
    public RedirectView addHotel(HotelDto hotelDto, HttpServletRequest request, RedirectAttributes redirectAttr) {
        tripService.addHotel(hotelDto);
        redirectAttr.addAttribute("info", "info");
        request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.TEMPORARY_REDIRECT);
        return new RedirectView("/trip/planner");
    }
}
