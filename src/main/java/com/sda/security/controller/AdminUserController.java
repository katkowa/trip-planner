package com.sda.security.controller;

import com.sda.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.PathParam;

@Controller
@RequestMapping(path = "/admin/user/")
public class AdminUserController {
    @Autowired
    private UserService userService;

    @Transactional
    @GetMapping(path = "/list")
    public String getUserList(Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        return "UserList";
    }

    @PostMapping("/delete")
    public String deleteUser(@PathParam(value = "userId") Long userId) {
        userService.deleteUserById(userId);
        return "redirect:/admin/user/list";
    }


    @PostMapping("/make-admin")
    public String makeAdmin(@RequestParam Long userId) {
        userService.makeAdmin(userId);
        return "redirect:/admin/user/list";
    }
}
