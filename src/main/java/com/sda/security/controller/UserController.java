package com.sda.security.controller;

import com.sda.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(path = "/register")
    public String getRegisterForm() {
        return "UserRegister";
    }

    @PostMapping(path = "/register")
    public String submitRegisterForm(@RequestParam(name="username") String username,
                                     @RequestParam(name="password") String password,
                                     @RequestParam(name="password-confirm") String passwordConfirm) {
        userService.registerUser(username, password, passwordConfirm);
        return "redirect:/login";
    }

}
