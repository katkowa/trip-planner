package com.sda.security.controller;

import com.sda.security.model.Trip;
import com.sda.security.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(path = "/")
public class IndexController {
    private final TripService tripService;

    @Autowired
    public IndexController(final TripService tripService) {
        this.tripService = tripService;
    }

    @GetMapping("/")
    public String getAll(Model model) {
        List<Trip> tripList = tripService.getAll();
        model.addAttribute("tripList", tripList);
        return "index";
    }


    @GetMapping(path = "/login")
    public String loginForm() {
        return "LoginForm";
    }
}
