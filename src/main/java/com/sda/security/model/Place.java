package com.sda.security.model;

import lombok.Getter;

@Getter
public enum Place {
    BLR("Bangalore", 13.023577, 77.536856, 12.923210, 77.642256),
    BCN("Barcelona", 41.42, 2.11, 41.347463, 2.228208),
    BER("Berlin", 52.541755, 13.354201, 52.490569, 13.457198),
    DFW("Dallas", 32.806993, -96.836857, 32.740310, -96.737293),
    LON("Londyn", 51.520180, -0.169882, 51.484703, -0.061048),
    NYC("New York", 40.792027, -74.058204, 40.697607, -73.942847),
    PAR("Paris", 48.91, 2.25, 48.80, 2.46),
    SFO("SanFrancisco", 37.810980, -122.483716, 37.732007, -122.370076);

    private String name;
    private double north;
    private double west;
    private double south;
    private double east;

    Place(final String name, final double north, final double west, final double south, final double east) {
        this.name = name;
        this.north = north;
        this.west = west;
        this.south = south;
        this.east = east;
    }

}
