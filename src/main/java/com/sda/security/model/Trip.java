package com.sda.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(columnDefinition = "varchar(11) default 'TO_DO'")
    @Enumerated(EnumType.STRING)
    private Status status = Status.TO_DO;
    private int numberOfPersons;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
    @Enumerated(EnumType.STRING)
    private Place place;
    private double totalPrice;
    @ManyToOne
    private Hotel hotel;
    @ManyToOne
    private Flight flightTo;
    @ManyToOne
    private Flight flightBack;
    @ManyToOne(fetch = FetchType.EAGER)
    private AppUser user;

    public void setTotalPrice() {
        double totalPrice = 0;
        totalPrice += getFlightBack() != null ? getFlightBack().getPrice() : 0;
        totalPrice += getFlightTo() != null ? getFlightTo().getPrice() : 0;
        totalPrice += getHotel() != null ? getHotel().getPrice() : 0;
        setTotalPrice(totalPrice);
    }
}


