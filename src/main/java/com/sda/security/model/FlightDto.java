package com.sda.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightDto {
    private Long tripId;
    private String departurePlace;
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private String departureTime;
    private String arrivalPlace;
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private String arrivalTime;
    private double price;
    private int numberOfChanges;

//    public Flight toFlight() {
//        Flight flight = new Flight();
//        flight.setDeparturePlace(this.departurePlace);
//        flight.setDepartureTime(this.departureTime);
//        flight.setArrivalPlace(this.arrivalPlace);
//        flight.setArrivalTime(this.arrivalTime);
//        flight.setPrice(this.price);
//        flight.setNumberOfChanges(this.numberOfChanges);
//        return flight;
//    }
}
