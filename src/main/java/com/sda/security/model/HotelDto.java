package com.sda.security.model;

import lombok.Data;

@Data
public class HotelDto {
     private Long tripId;
     private String name;
     private String description;
     private String address;
     private Place place;
     private double price;
     private double latitude;
     private double longitude;
}
