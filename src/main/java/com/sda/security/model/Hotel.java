package com.sda.security.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Hotel {
    @Id
    @GeneratedValue
    private Long id;
    private Place place;
    private String name;
    private String address;
    private double latitude;
    private double longitude;
    private double price;
    @Column(columnDefinition="varchar(500)")
    private String description;


    @Override
    public String toString() {
        return name + ", " + address;
    }
}
