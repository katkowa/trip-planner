package com.sda.security.model;

public enum Status {
    TO_DO, IN_PROGRESS, DONE
}
