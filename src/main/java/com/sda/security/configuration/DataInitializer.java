package com.sda.security.configuration;

import com.sda.security.model.*;
import com.sda.security.repository.AppUserRepository;
import com.sda.security.repository.TripRepository;
import com.sda.security.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    private final UserRoleRepository userRoleRepository;
    private final AppUserRepository appUserRepository;
    private final PasswordEncoder passwordEncoder;
    private final TripRepository tripRepository;

    @Autowired
    public DataInitializer(final UserRoleRepository userRoleRepository, final AppUserRepository appUserRepository, final PasswordEncoder passwordEncoder, final TripRepository tripRepository) {
        this.userRoleRepository = userRoleRepository;
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.tripRepository = tripRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        createRoleIfNotExist("ADMIN");
        createRoleIfNotExist("USER");

        createUserWithRoleIfNotExist("admin", "admin", "ADMIN", "USER");
        createUserWithRoleIfNotExist("user", "user", "USER");

//        createTrips();
    }

    private void createTrips() {
        AppUser user = appUserRepository.findByEmail("user").get();
        Trip trip = new Trip();
        trip.setUser(user);
        trip.setPlace(Place.BCN);
        trip.setStartDate(LocalDate.of(2019, 7, 11));
        trip.setEndDate(LocalDate.of(2019, 7, 18));
        trip.setStatus(Status.IN_PROGRESS);
        user.getTrips().add(trip);
        tripRepository.save(trip);

        trip = new Trip();
        trip.setUser(user);
        trip.setPlace(Place.BLR);
        trip.setStartDate(LocalDate.of(2019, 9, 20));
        trip.setEndDate(LocalDate.of(2019, 9, 24));
        user.getTrips().add(trip);
        tripRepository.save(trip);

        appUserRepository.save(user);
    }

    private void createUserWithRoleIfNotExist(String username, String password, String... roles) {
        if (!appUserRepository.existsByEmail(username)) {
            AppUser appUser = new AppUser();
            appUser.setEmail(username);
            appUser.setPassword(passwordEncoder.encode(password));

            appUser.setRoles(new HashSet<>(findRoles(roles)));

            appUserRepository.save(appUser);
        }
    }

    private List<UserRole> findRoles(String[] roles) {
        List<UserRole> userRoles = new ArrayList<>();

        for (String role : roles) {
            userRoles.add(userRoleRepository.findByName(role));
        }
        return userRoles;
    }

    private void createRoleIfNotExist(String roleName) {
        if (!userRoleRepository.existsByName(roleName)) {
            UserRole role = new UserRole();
            role.setName(roleName);

            userRoleRepository.save(role);
        }
    }


}
