package com.sda.security.repository;

import com.sda.security.model.Status;
import com.sda.security.model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TripRepository extends JpaRepository<Trip, Long> {
    List<Trip> findByUserEmailAndStatus(String email, Status status);

    List<Trip> findByUserId(Long userId);
}
