package com.sda.security.exception;

public class PasswordDoNotMatchException extends RuntimeException {
    public PasswordDoNotMatchException(final String message) {
        super(message);
    }
}
